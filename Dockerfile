FROM php:7.4-apache
RUN apt-get update
RUN apt-get install -y alpine-pico libsqlite3-dev
RUN apt-get install -y libpng-dev zlib1g-dev
RUN apt-get install -y libonig-dev
RUN apt-get install -y libzip-dev zip
RUN docker-php-ext-install pdo_mysql 
RUN docker-php-ext-install pdo_sqlite
RUN docker-php-ext-install mysqli
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install zip
RUN docker-php-ext-install gd
RUN a2enmod rewrite
RUN cp -f "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"
#RUN useradd -m perazzo
#RUN apt-get update
#RUN apt-get install -y python python-pip python-dev build-essential g++ libapache2-mod-wsgi python$
#RUN pip install numpy pandas sympy matplotlib werkzeug==0.16.1 Flask lxml mysqlclient unidecode re$
RUN sed -i 's/display_errors = On/;display_errors = Off/g' "$PHP_INI_DIR/php.ini"
ENV TZ=America/Fortaleza
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN cat $PHP_INI_DIR/php.ini-production > $PHP_INI_DIR/php.ini
RUN echo "date.timezone = America/Fortaleza" >> /usr/local/etc/php/php.ini
RUN echo "display_errors = On" >> /usr/local/etc/php/php.ini
