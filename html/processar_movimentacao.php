<?php
      include('iniciar.php');
      session_start();
      $api = 0; //Se o usuário chega através  da API, valor = 1; Senão valor = 0
      $continua = 1;
      $usuario = "INDEFINIDO";
      if (!isset($_SESSION['autenticado'])) {
        if (!isset($_POST['token'])) {
          header('Location: login.html'); //Usuário não autenticado pelo sistema web
        }
      }

      if (isset($_POST['token'])) { //Requisição chegando da API
        $token = $_POST['token'];
        $api = 1;
        $linhas = $db->select("users",["username","nome"],["token"=>$token]);
        if (count($linhas)==0) {
          $continua = 0;
        }
        else {
          foreach ($linhas as $linha) {
            $usuario = $linha['username'];
          }
        }
      }
      if ($continua==1) {
        if (isset($_POST['item'])) {
          $item = $_POST['item'];
          $quantidade = $_POST['quantidade'];
          $setor = $_POST['ua'];
          $finalidade = $_POST['finalidade'];
          $descricao = $_POST['descricao'];
          $data = date("Y-m-d H:i:s");
          $medida = $_POST['medida'];
          if ($api==0) {
            $usuario = $_SESSION['usuario'];
          }
          $db->insert("movimentacao",["item"=>$item,"quantidade"=>$quantidade,"finalidade"=>$finalidade,"descricao"=>$descricao,"data"=>$data,"username"=>$usuario,"unidade"=>$setor,"medida"=>$medida]);
          $codigo = $db->id();
          if ($api==0) {
            header('Location: inserir_movimentacao.php');
          }
          else {
            print("200");
          }
        } 
        else {
          print("ERRO: Informar ao desenvolvedor!");
        }
      }
      else { //Token inválido!
        print("403");
      }
      

?> 