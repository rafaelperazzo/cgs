<?php     
      include('iniciar.php');
      session_start();
      error_reporting(E_ALL);
      ini_set('display_errors', '1');
      $api = 0; //Se o usuário chega através  da API, valor = 1; Senão valor = 0
      $continua = 1;
      $usuario = "INDEFINIDO";
      if (!isset($_SESSION['autenticado'])) {
        if (!isset($_POST['token'])) {
          header('Location: login.html'); //Usuário não autenticado pelo sistema web
        }
      }

      if (isset($_POST['token'])) { //Requisição chegando da API
        $token = $_POST['token'];
        $api = 1;
        $linhas = $db->select("users",["username","nome"],["token"=>$token]);
        if (count($linhas)==0) {
          $continua = 0;
        }
        else {
          foreach ($linhas as $linha) {
            $usuario = $linha['username'];
          }
        }
      }
      if ($continua==1) {
        if (isset($_POST['item'])) {
          $item = $_POST['item'];
          $operacao = $_POST['operacao'];
          $tipo = $_POST['tipo_operacao'];
          $cpf = $_POST['cpf_fornecedor'];
          $razao_social = $_POST['razao_social_fornecedor'];
          $numero_nf = $_POST['numero_nf'];
          $data_nf = $_POST['data_nf'];
          $quantidade = $_POST['quantidade'];
          $medida = $_POST['medida'];
          $densidade = $_POST['densidade'];
          $concentracao = $_POST['concentracao'];
          $ua = $_POST['ua'];
          $nome_laboratorio = $_POST['nome_laboratorio'];
          $responsavel = $_POST['responsavel'];
          /*
          Salvando os arquivos anexados
          */
          $uploaddir='/var/www/share/';
          $file_token = generateRandomString(14);
          $filename1 = 'arquivo1_' . $file_token . '.pdf';
          if(!file_exists($_FILES['arquivo1']['tmp_name']) || !is_uploaded_file($_FILES['arquivo1']['tmp_name'])) {
            $arquivo1="";
          }
          else {
            $arquivo1 = $filename1;
            $uploadfile1 = $uploaddir . $filename1;
            move_uploaded_file($_FILES['arquivo1']['tmp_name'], $uploadfile1);
          }
          if(!file_exists($_FILES['arquivo2']['tmp_name']) || !is_uploaded_file($_FILES['arquivo2']['tmp_name'])) {
            $arquivo2 = "";
          }
          else {
            $filename2 = 'arquivo2_' . $file_token . '.pdf';
            $arquivo2 = $filename2;
            $uploadfile2 = $uploaddir . $filename2;
            move_uploaded_file($_FILES['arquivo2']['tmp_name'], $uploadfile2);
          }
          if ($api==0) {
            $usuario = $_SESSION['usuario'];
          }
          $db->insert("aquisicao",["item"=>$item,"operacao"=>$operacao,"tipo"=>$tipo,"identificador"=>$cpf,"razao_social"=>$razao_social,"nota_fiscal"=>$numero_nf,"data_nota_fiscal"=>$data_nf,"quantidade"=>$quantidade,"medida"=>$medida,"densidade"=>$densidade,"concentracao"=>$concentracao,"unidade"=>$ua,"nome_laboratorio"=>$nome_laboratorio,"responsavel"=>$responsavel,"documento1"=>$arquivo1,"documento2"=>$arquivo2,"username"=>$usuario]);
          $codigo = $db->id();
          if ($api==0) {
            header('Location: inserir_aquisicao.php');
          }
          else {
            print("200");
          }
        } 
        else {
          print("ERRO: Informar ao desenvolvedor!");
        }
      }
      else { //Token inválido!
        print("403");
      }
      

?> 