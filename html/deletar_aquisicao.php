<?php

include('iniciar.php');

if (!isset($_GET['token'])) {
    include('sessao.php');
    if (isset($_GET['id'])) {
        $usuario = $_SESSION['usuario'];
        $linhas = $db->select("aquisicao",["id"],["username"=>$usuario,"id"=>$_GET['id']]);
        $id = $_GET['id'];
        if (count($linhas)>0) {
            $linhas_afetadas = $db->delete("aquisicao",["AND"=>["id"=>$id]]);
        }
        header('Location: inserir_aquisicao.php');
        
    }
    else {
        header('Location: inserir_aquisicao.php');
    }
}
else  {
    #Verifica se o token é valido
    $token = $_GET['token'];
    if (tokenValido($db,$token)) {
        $usuario = token2Field($db,$token,"username");
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $linhas = $db->select("aquisicao",["id"],["username"=>$usuario,"id"=>$_GET['id']]);
            if (count($linhas)>0) {
                $linhas_afetadas = $db->delete("aquisicao",["AND"=>["id"=>$id]]);
                $resposta = $linhas_afetadas->rowCount();
                if ($resposta>0) {
                    print("200");
                }
                else {
                    print("404");
                }
            }
            else {
                print("ID INEXISTENTE OU SEM PERMISSAO");
            }
        }
    }
    else {
        print("TOKEN INVALIDO");
    }
}


?>