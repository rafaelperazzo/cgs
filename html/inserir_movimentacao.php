<?php
      include('iniciar.php');
      session_start();
      if (!isset($_SESSION['autenticado'])) {
        header('Location: login.html'); 
      }
    ?>  
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js" integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    
    <title>Cadastrar Movimentação</title>
    <script>

      function strReplace(){

          var myStr = document.getElementById("quantidade").value;

          var newStr = myStr.replace(",", ".");

          
          // Insert modified string in paragraph

          document.getElementById("quantidade").value = newStr;

      }

    </script>
    <style>
        .responsive {
            width: 100%;
            height: auto;
        }
    </style>
  </head>
  <body>
  
  <img src="sub_logo_sci02.png" alt="lOGO" class="responsive">
  <center><h1>Cadastrar Movimentação</h1></center>
    
    <form action="processar_movimentacao.php" method="POST">

        <div class="form-group">
            <label for="item">Item</label>
            <select class="form-control" id="item" name="item" required>
            <?php
                $linhas = $db->select("items",["id","descricao"],["ORDER"=>["descricao"=>"ASC"]]);
                foreach ($linhas as $linha) {
                  $descricao = $linha['descricao'];
                  $id = $linha['id'];
                  print("<option value='$id'>$descricao</option>");
                }
              ?>
            </select>
        </div>

        


        <div class="form-group">
          <label for="quantidade">Quantidade</label>
          <input type="text" class="form-control" id="quantidade" name="quantidade" aria-describedby="quantidadeHelp" pattern="^[0-9]+(\.?[0-9]+)*$" onblur="strReplace()" required>
          <small id="quantidadeHelp" class="form-text text-muted">Quantidade utilizada do produto. Apenas números.</small>
        </div>
        
        <div class="form-group">
            <label for="medida">Unidade</label>
            <select class="form-control" id="medida" name="medida" required>
            <option value="KG">Kg (kilogramas)</option>
            <option value="L">L (litros)</option>
            </select>
        </div>

        <div class="form-group">
            <label for="ua">Setor</label>
            <select class="form-control" id="ua" name="ua" required>
            <option value="CCT">CCT</option>
            <option value="CCSA">CCSA</option>
            <option value="CCAB">CCAB</option>
            <option value="IISCA">IISCA</option>
            <option value="IFE">IFE</option>
            <option value="FAMED">FAMED</option>
            </select>
        </div>

        <div class="form-group">
            <label for="finalidade">Finalidade</label>
            <select class="form-control" id="finalidade" name="finalidade" required>
            <option value="PESQUISA">PROJETO DE PESQUISA</option>
            <option value="AULA PRÁTICA">AULA PRÁTICA</option>
            <option value="TRABALHO DE CONCLUSÃO DE CURSO">TRABALHO DE CONCLUSÃO DE CURSO</option>
            <option value="MONOGRAFIA, DISSERTAÇÃO, TESE">MONOGRAFIA, DISSERTAÇÃO, TESE</option>
            <option value="OUTRO">OUTRO (DESCREVER em detalhes)</option>
            </select>
        </div>

        <div class="form-group">
          <label for="descricao">Detalhes da finalidade</label>
          <textarea class="form-control" id="descricao" name="descricao" rows="3" required></textarea>
        </div>


        <button type="submit" class="btn btn-primary">Cadastrar</button>

    </form>
<hr>
<h2>Movimentações registradas</h2>
  <table class="display nowrap" style="width:100%" id="tab01">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">ITEM</th>
      <th scope="col">QUANTIDADE</th>
      <th scope="col">MEDIDA</th>
      <th scope="col">FINALIDADE</th>
      <th scope="col">DESCRIÇÃO</th>
      <th scope="col">DATA</th>
      <th scope="col">USUÁRIO</th>
      <th scope="col">SETOR</th>
      <th scope="col">REMOVER</th>
    </tr>
  </thead>
  <tbody>
    
    <?php
      
      $usuario = $_SESSION['usuario'];
      //$linhas = $db->select("movimentacao",["[>]items"=>["item"=>"id"],"[>]medidas"=>["item"=>"id"]],["movimentacao.id","items.descricao(descricao_item)","movimentacao.quantidade","movimentacao.finalidade","movimentacao.descricao","movimentacao.data","medidas.descricao(descricao_medida)","movimentacao.username"],["username"=>$usuario,"ORDER"=>["movimentacao.id"=>"ASC"]]);
      $consulta = "SELECT movimentacao.id as id,items.descricao as descricao_item,movimentacao.quantidade,movimentacao.medida as descricao_medida,finalidade,movimentacao.descricao,movimentacao.data,movimentacao.username,movimentacao.unidade FROM movimentacao,items WHERE movimentacao.item=items.id ORDER BY movimentacao.data";
      $linhas = $db->query($consulta)->fetchAll();
      //print(json_encode($linhas));
      
      foreach ($linhas as $linha) {  
    ?>
    <tr>
      <?php
        $id = $linha['id'];
        $item = $linha['descricao_item'];
        $quantidade = $linha['quantidade'];
        $medida = $linha['descricao_medida'];
        $finalidade = $linha['finalidade'];
        $descricao = $linha['descricao'];
        $data = new DateTime($linha['data']);
        $data = $data->format('d/m/Y H:i:s');
        $usuario = $linha['username'];
        $unidade = $linha['unidade'];
        print("<td>$id</td>");
        print("<td>$item</td>");
        print("<td>$quantidade</td>");
        print("<td>$medida</td>");
        print("<td>$finalidade</td>");
        print("<td>$descricao</td>");
        print("<td>$data</td>");
        print("<td>$usuario</td>");
        $confirmacao = "Tem certeza de que deseja remover esta movimentação ?";
        print("<td>$unidade</td>");
        print("<td><center><a href='deletar_movimentacao.php?id=$id' onclick=\"return confirm('$confirmacao')\"><img src='remove64.png'></a></center></td>");
      ?>
    </tr>
    <?php
      }
    ?>

  </tbody>
</table>
           
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>

<script>
        $(document).ready(function() {
          $('#tab01').DataTable( {
              dom: 'Bfrtip',
              buttons: [
                  'copy', 'csv', 'excel', 'pdf', 'print'
              ]
          } );
      } );
</script>

    

    <script>
        $(document).ready(function(){
            
            // Initialize select2
            $("#item").select2();
            $("#ua").select2();
        });
        </script>
<?php
include('footer.php');
?>

  </body>
</html>
