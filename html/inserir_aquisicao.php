<?php
      include('iniciar.php');
      session_start();
      if (!isset($_SESSION['autenticado'])) {
        header('Location: login.html'); 
      }
    ?>  
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js" integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    
    
    <title>Cadastrar Aquisição</title>
    <script>

      function strReplace(){

          var myStr = document.getElementById("quantidade").value;

          var newStr = myStr.replace(",", ".");

          
          // Insert modified string in paragraph

          document.getElementById("quantidade").value = newStr;

      }

    </script>
    <style>
        .responsive {
            width: 100%;
            height: auto;
        }
    </style>

    <script>
      function tipo_operacao() {
          var x = document.forms["frmAquisicao"]["operacao"].value;

          if (x == "1") { //SAÍDA
                document.forms["frmAquisicao"]["arquivo2"].disabled = true;
                document.forms["frmAquisicao"]["arquivo2"].required = false;
            }
        else { //ENTRADA
                document.forms["frmAquisicao"]["arquivo2"].disabled = false;
                document.forms["frmAquisicao"]["arquivo2"].required = true;
        }
        
      }

      function lista_opcoes() {
        var arrOptions1 = [];
        var arrOptions2 = [];
        var entrada = document.forms["frmAquisicao"]["operacao"].value;
        arrOptions1.push("<option value='COMPRA'>COMPRA</option>");
        arrOptions1.push("<option value='RECEBIMENTO DE TRANSFERÊNCIA'>RECEBIMENTO DE TRANSFERÊNCIA</option>");
        arrOptions1.push("<option value='RECEBIMENTO DE DOAÇÃO'>RECEBIMENTO DE DOAÇÃO</option>");
        arrOptions1.push("<option value='RECEBIMENTO DE PRODUTO ARMAZENADO'>RECEBIMENTO DE PRODUTO ARMAZENADO</option>");
        arrOptions1.push("<option value='RECEBIMENTO DE PRODUTO PARA INDUSTRIALIZAÇÃO'>RECEBIMENTO DE PRODUTO PARA INDUSTRIALIZAÇÃO</option>");
        arrOptions1.push("<option value='OUTROS RECEBIMENTOS'>OUTROS RECEBIMENTOS</option>");
        arrOptions2.push("<option value='DOAÇÃO'>DOAÇÃO</option>");
        arrOptions2.push("<option value='TRANSFERÊNCIA'>TRANSFERÊNCIA</option>");
        arrOptions2.push("<option value='DEVOLUÇÃO/RETORNO DE PRODUTO INDUSTRIALIZADO'>DEVOLUÇÃO/RETORNO DE PRODUTO INDUSTRIALIZADO</option>");
        arrOptions2.push("<option value='REMESSA PARA ARMAZENAGEM'>REMESSA PARA ARMAZENAGEM</option>");
        arrOptions2.push("<option value='REMESSA DE PRODUTO PARA INDUSTRIALIZAÇÃO'>REMESSA DE PRODUTO PARA INDUSTRIALIZAÇÃO</option>");
        arrOptions2.push("<option value='OUTRAS REMESSAS'>OUTRAS REMESSAS</option>");
        if (entrada=="1") {
          document.forms["frmAquisicao"]["tipo_operacao"].innerHTML = arrOptions2.join();
        }
        else {
          document.forms["frmAquisicao"]["tipo_operacao"].innerHTML = arrOptions1.join();
        }
        
        
      }

      function checagem() {
        //tipo_operacao();
        lista_opcoes();
      }

    </script>

  </head>
  <body onpageshow="checagem();">
  
  <img src="sub_logo_sci02.png" alt="lOGO" class="responsive">
  <center><h1>Cadastrar Aquisição</h1></center>
    
    <form name="frmAquisicao" action="processar_aquisicao.php" method="POST">
        <div class="form-group">
            <p>Operação</p>
            <input type="radio" id="entrada" name="operacao" value="0" onchange="checagem();" checked>
            <label for="entrada">ENTRADA</label><br>
            <input type="radio" id="saida" name="operacao" value="1" onchange="checagem();">
            <label for="saida">SAÍDA</label><br>
        </div>
        
        <div>
          <label for="tipo_operacao">Tipo</label>
          <select class="form-control" id="tipo_operacao" name="tipo_operacao" onfocus="lista_opcoes();" required>
      
          </select>

        </div>

        <div class="form-group">
        <label for="cpf_fornecedor">CPF/CNPJ do fornecedor</label>
          <input type="text" class="form-control" id="cpf_fornecedor" name="cpf_fornecedor" onblur="strReplace()" required>
        </div>
        <div>
        <label for="razao_social_fornecedor">Razão Social/Nome adquirente/Fornecedor</label>
          <input type="text" class="form-control" id="razao_social_fornecedor" name="razao_social_fornecedor" required>
        </div>
        <div>
        <label for="numero_nf">Número da Nota Fiscal</label>
          <input type="text" class="form-control" id="numero_nf" name="numero_nf" required>
        </div>
        <div>
        <label for="data_nf">Data da nota fiscal</label>
          <input type="date" class="form-control" id="data_nf" name="data_nf" required>
        </div>
        <div class="form-group">
            <label for="item">Item</label>
            <select class="form-control" id="item" name="item" required>
            <?php
                $linhas = $db->select("items",["id","descricao"],["ORDER"=>["descricao"=>"ASC"]]);
                foreach ($linhas as $linha) {
                  $descricao = $linha['descricao'];
                  $id = $linha['id'];
                  print("<option value='$id'>$descricao</option>");
                }
              ?>
            </select>
        </div>
        
        <div class="form-group">
          <label for="quantidade">Quantidade</label>
          <input type="text" class="form-control" id="quantidade" name="quantidade" aria-describedby="quantidadeHelp" pattern="^[0-9]+(\.?[0-9]+)*$" onblur="strReplace()" required>
          <small id="quantidadeHelp" class="form-text text-muted">Quantidade movimentada do produto. Apenas números.</small>
        </div>
        
        <div class="form-group">
            <label for="medida">Unidade</label>
            <select class="form-control" id="medida" name="medida" required>
            <option value="KG">Kg (kilogramas)</option>
            <option value="L">L (litros)</option>
            </select>
        </div>

        <div class="form-group">
          <label for="densidade">Densidade</label>
          <input type="text" class="form-control" id="densidade" name="densidade" aria-describedby="densidadeHelp" required>
          <small id="densidadeHelp" class="form-text text-muted">A densidade deve ser expressa em kilogramas por litro conforme Nota Técnica n° 02/2019-UPTC/NUCOP/DCPQ/CGCSP/DIREX/PF</small>
        </div>
        
        <div class="form-group">
          <label for="concentracao">Concentração do Produto</label>
          <input type="text" class="form-control" id="concentracao" name="concentracao" aria-describedby="concentracaoHelp" required>
          <small id="concentracaoHelp" class="form-text text-muted">A concentração deve ser expressa em porcentagem conforme Nota Técnica n° 02/2019-UPTC/NUCOP/DCPQ/CGCSP/DIREX/PF</small>
        </div>

        

        <div class="form-group">
            <label for="ua">Unidade Acadêmica</label>
            <select class="form-control" id="ua" name="ua" required>
            <option value="CCT">CCT</option>
            <option value="CCSA">CCSA</option>
            <option value="CCAB">CCAB</option>
            <option value="IISCA">IISCA</option>
            <option value="IFE">IFE</option>
            <option value="FAMED">FAMED</option>
            </select>
        </div>

        <div>
        <label for="nome_laboratorio">Nome do Laboratório</label>
          <input type="text" class="form-control" id="nome_laboratorio" name="nome_laboratorio" required>
        </div>

        <div>
        <label for="responsavel">Responsável</label>
          <input type="text" class="form-control" id="responsavel" name="responsavel" required>
        </div>
        <h3>Anexar documento(s) comprobatório(s)</h3>
        <div>
          <label for="arquivo1">Nota Fiscal</label><br>
          <input type="file" id="arquivo1" name="arquivo1" enctype="multipart/form-data">
        </div>
        
        <div>
          <label for="arquivo2">Termo de Doação</label><br>
          <input type="file" id="arquivo2" name="arquivo2" enctype="multipart/form-data">
          <small id="arquivo2" class="form-text text-muted">Também obrigatória a inserção da Nota Fiscal do produto</small>
        </div>
        <br>
        <button formenctype="multipart/form-data" type="submit" class="btn btn-primary">Cadastrar</button>

    </form>
<hr>
<h2>Movimentações registradas</h2>
  <table class="display nowrap" style="width:100%" id="tab01">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">ITEM</th>

      <th scope="col">OPERAÇÃO</th>
      <th scope="col">TIPO DE OPERAÇÃO</th>
      <th scope="col">CPF/CNPJ FORNECEDOR</th>
      <th scope="col">RAZÃO SOCIAL</th>
      <th scope="col">NF</th>
      <th scope="col">DATA DA NF</th>

      <th scope="col">QUANTIDADE</th>
      <th scope="col">MEDIDA</th>

      <th scope="col">DENSIDADE</th>
      <th scope="col">CONCENTRAÇÃO</th>
      <th scope="col">UNIDADE ACADÊMICA</th>
      <th scope="col">LABORATÓRIO</th>
      <th scope="col">RESPONSÁVEL</th>
      <th scope="col">NF</th>
      <th scope="col">TERMO DE DOAÇÃO</th>

      <th scope="col">DATA</th>
      <th scope="col">USUÁRIO</th>
      <th scope="col">REMOVER</th>
    </tr>
  </thead>
  <tbody>
    
    <?php
      
      $usuario = $_SESSION['usuario'];
      //$linhas = $db->select("movimentacao",["[>]items"=>["item"=>"id"],"[>]medidas"=>["item"=>"id"]],["movimentacao.id","items.descricao(descricao_item)","movimentacao.quantidade","movimentacao.finalidade","movimentacao.descricao","movimentacao.data","medidas.descricao(descricao_medida)","movimentacao.username"],["username"=>$usuario,"ORDER"=>["movimentacao.id"=>"ASC"]]);
      $consulta = "SELECT aquisicao.id as id,items.descricao as descricao_item,aquisicao.quantidade,aquisicao.medida as descricao_medida,aquisicao.data,aquisicao.username,aquisicao.unidade, aquisicao.tipo,aquisicao.operacao,aquisicao.identificador,aquisicao.razao_social,aquisicao.nota_fiscal,aquisicao.data_nota_fiscal,aquisicao.medida,aquisicao.densidade,aquisicao.concentracao,aquisicao.nome_laboratorio,aquisicao.documento1,aquisicao.documento2,aquisicao.responsavel FROM aquisicao,items WHERE aquisicao.item=items.id ORDER BY aquisicao.data DESC";
      $linhas = $db->query($consulta)->fetchAll();
      //print(json_encode($linhas));
      
      foreach ($linhas as $linha) {  
    ?>
    <tr>
      <?php
        $id = $linha['id'];
        $item = $linha['descricao_item'];
        $quantidade = $linha['quantidade'];
        $medida = $linha['medida'];
        $data = new DateTime($linha['data']);
        $data = $data->format('d/m/Y H:i:s');
        $usuario = $linha['username'];
        $unidade = $linha['unidade'];
        print("<td>$id</td>");
        print("<td>$item</td>");

        $operacao = $linha['operacao'];
        if ($operacao=="0") {
            $operacao = "ENTRADA";
        }
        else {
          $operacao = "SAÍDA";
        }
        $tipo = $linha['tipo'];
        $identificador = $linha['identificador'];
        $razao_social = $linha['razao_social'];
        $nota_fiscal = $linha['nota_fiscal'];
        $data_nota_fiscal = $linha['data_nota_fiscal'];
        $df = new DateTime($linha['data_nota_fiscal']);
        $data_nota_fiscal = $df->format('d/m/Y');
        print("<td>$operacao</td>");
        print("<td>$tipo</td>");
        print("<td>$identificador</td>");
        print("<td>$razao_social</td>");
        print("<td>$nota_fiscal</td>");
        print("<td>$data_nota_fiscal</td>");

        print("<td>$quantidade</td>");
        print("<td>$medida</td>");

        $densidade = $linha['densidade'];
        $concentracao = $linha['concentracao'];
        $nome_laboratorio = $linha['nome_laboratorio'];
        $responsavel = $linha['responsavel'];
        $arquivo1 = $linha['documento1'];
        $arquivo2 = $linha['documento2'];
        print("<td>$densidade</td>");
        print("<td>$concentracao</td>");
        print("<td>$unidade</td>");
        print("<td>$nome_laboratorio</td>");
        print("<td>$responsavel</td>");
        if ($arquivo1!="") {
          print("<td><a href='send_file.php?arquivo=$arquivo1'>NOTA FISCAL</a></td>");
        }
        else {
          print("<td>N/A</td>");
        }
        if ($arquivo2!="") {
          print("<td><a href='send_file.php?arquivo=$arquivo2'>TERMO DE DOAÇÃO</a></td>");
        }
        else {
          print("<td>N/A</td>");
        }
        
        print("<td>$data</td>");
        print("<td>$usuario</td>");
        $confirmacao = "Tem certeza de que deseja remover esta movimentação ?";
        
        print("<td><center><a href='deletar_aquisicao.php?id=$id' onclick=\"return confirm('$confirmacao')\"><img src='remove64.png'></a></center></td>");
      ?>
    </tr>
    <?php
      }
    ?>

  </tbody>
</table>
    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>

<script>
        $(document).ready(function() {
          $('#tab01').DataTable( {
              dom: 'Bfrtip',
              buttons: [
                  'csv', 'excel'
              ]
          } );
      } );
</script>
    

    <script>
        $(document).ready(function(){
            
            // Initialize select2
            $("#item").select2();
            $("#ua").select2();
        });
        </script>
<?php
include('footer.php');
?>

  </body>
</html>
