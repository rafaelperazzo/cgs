<?php
      include('iniciar.php');
      session_start();
      if (!isset($_SESSION['autenticado'])) {
        header('Location: login.html'); 
      }
?>  

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/select2.min.js" integrity="sha512-2ImtlRlf2VVmiGZsjm9bEyhjGW4dU7B6TNwh/hx/iSByxNENtj3WVE6o/9Lj4TJeVXPi4bnOIMXFIJJAeufa0A==" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/js/i18n/pt-BR.min.js" integrity="sha512-H1yBoUnrE7X+NeWpeZvBuy2RvrbvLEAEjX/Mu8L2ggUBja62g1z49fAboGidE5YEQyIVMCWJC9krY4/KEqkgag==" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.13/css/select2.min.css" integrity="sha512-nMNlpuaDPrqlEls3IX/Q56H36qvBASwb3ipuo3MxeWbsQB1881ox0cRv7UPTgBlriqoynt35KjEwgGUeUXIPnw==" crossorigin="anonymous" />
    
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
    <style type="text/css" media="print">
        @page { 
            size: landscape;
        }
        body { 
            writing-mode: tb-rl;
        }
    </style>
    
    <title>AQUISIÇÕES</title>
    </head>
  <body>
  
  <img src="sub_logo_sci02.png" alt="lOGO" class="responsive">
  <center><h1>AQUISIÇÕES</h1></center>
  <h2>Movimentações registradas</h2>
  <table class="display nowrap" style="width:100%" id="tab01">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">ITEM</th>

      <th scope="col">OPERAÇÃO</th>
      <th scope="col">TIPO DE OPERAÇÃO</th>
      <th scope="col">CPF/CNPJ FORNECEDOR</th>
      <th scope="col">RAZÃO SOCIAL</th>
      <th scope="col">NF</th>
      <th scope="col">DATA DA NF</th>

      <th scope="col">QUANTIDADE</th>
      <th scope="col">MEDIDA</th>

      <th scope="col">DENSIDADE</th>
      <th scope="col">CONCENTRAÇÃO</th>
      <th scope="col">UNIDADE ACADÊMICA</th>
      <th scope="col">LABORATÓRIO</th>
      <th scope="col">RESPONSÁVEL</th>
      <th scope="col">NF</th>
      <th scope="col">TERMO DE DOAÇÃO</th>

      <th scope="col">DATA</th>
      <th scope="col">USUÁRIO</th>
      <th scope="col">REMOVER</th>
    </tr>
  </thead>
  <tbody>
    
    <?php
      
      $usuario = $_SESSION['usuario'];
      //$linhas = $db->select("movimentacao",["[>]items"=>["item"=>"id"],"[>]medidas"=>["item"=>"id"]],["movimentacao.id","items.descricao(descricao_item)","movimentacao.quantidade","movimentacao.finalidade","movimentacao.descricao","movimentacao.data","medidas.descricao(descricao_medida)","movimentacao.username"],["username"=>$usuario,"ORDER"=>["movimentacao.id"=>"ASC"]]);
      $consulta = "SELECT aquisicao.id as id,items.descricao as descricao_item,aquisicao.quantidade,aquisicao.medida as descricao_medida,aquisicao.data,aquisicao.username,aquisicao.unidade, aquisicao.tipo,aquisicao.operacao,aquisicao.identificador,aquisicao.razao_social,aquisicao.nota_fiscal,aquisicao.data_nota_fiscal,aquisicao.medida,aquisicao.densidade,aquisicao.concentracao,aquisicao.nome_laboratorio,aquisicao.documento1,aquisicao.documento2,aquisicao.responsavel FROM aquisicao,items WHERE aquisicao.item=items.id ORDER BY aquisicao.data DESC";
      $linhas = $db->query($consulta)->fetchAll();
      //print(json_encode($linhas));
      
      foreach ($linhas as $linha) {  
    ?>
    <tr>
      <?php
        $id = $linha['id'];
        $item = $linha['descricao_item'];
        $quantidade = $linha['quantidade'];
        $medida = $linha['medida'];
        $data = new DateTime($linha['data']);
        $data = $data->format('d/m/Y H:i:s');
        $usuario = $linha['username'];
        $unidade = $linha['unidade'];
        print("<td>$id</td>");
        print("<td>$item</td>");

        $operacao = $linha['operacao'];
        if ($operacao=="0") {
            $operacao = "ENTRADA";
        }
        else {
          $operacao = "SAÍDA";
        }
        $tipo = $linha['tipo'];
        $identificador = $linha['identificador'];
        $razao_social = $linha['razao_social'];
        $nota_fiscal = $linha['nota_fiscal'];
        $data_nota_fiscal = $linha['data_nota_fiscal'];
        $df = new DateTime($linha['data_nota_fiscal']);
        $data_nota_fiscal = $df->format('d/m/Y');
        print("<td>$operacao</td>");
        print("<td>$tipo</td>");
        print("<td>$identificador</td>");
        print("<td>$razao_social</td>");
        print("<td>$nota_fiscal</td>");
        print("<td>$data_nota_fiscal</td>");

        print("<td>$quantidade</td>");
        print("<td>$medida</td>");

        $densidade = $linha['densidade'];
        $concentracao = $linha['concentracao'];
        $nome_laboratorio = $linha['nome_laboratorio'];
        $responsavel = $linha['responsavel'];
        $arquivo1 = $linha['documento1'];
        $arquivo2 = $linha['documento2'];
        print("<td>$densidade</td>");
        print("<td>$concentracao</td>");
        print("<td>$unidade</td>");
        print("<td>$nome_laboratorio</td>");
        print("<td>$responsavel</td>");
        if ($arquivo1!="") {
          print("<td><a href='send_file.php?arquivo=$arquivo1'>NOTA FISCAL</a></td>");
        }
        else {
          print("<td>N/A</td>");
        }
        if ($arquivo2!="") {
          print("<td><a href='send_file.php?arquivo=$arquivo2'>TERMO DE DOAÇÃO</a></td>");
        }
        else {
          print("<td>N/A</td>");
        }
        
        print("<td>$data</td>");
        print("<td>$usuario</td>");
        $confirmacao = "Tem certeza de que deseja remover esta movimentação ?";
        
        print("<td><center><a href='deletar_aquisicao.php?id=$id' onclick=\"return confirm('$confirmacao')\"><img src='remove64.png'></a></center></td>");
      ?>
    </tr>
    <?php
      }
    ?>

  </tbody>
</table>
    
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>

<script>
        $(document).ready(function() {
          $('#tab01').DataTable( {
              dom: 'Bfrtip',
              buttons: [
                  'csv', 'excel'
              ]
          } );
      } );
</script>
    

    <script>
        $(document).ready(function(){
            
            // Initialize select2
            $("#item").select2();
            $("#ua").select2();
        });
        </script>

</body>
</html>
