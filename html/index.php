<?php
  session_start();
?>
<!doctype html>

<html lang="pt_BR">

  <head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="robots" content="noindex">


    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- JS, Popper.js, and jQuery -->
  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

    <title>CONTROLE DE UTILIZAÇÃO DE ITENS CONTROLADOS</title>
    <style>
        .responsive {
            width: 100%;
            height: auto;
        }
    </style>
  </head>

  <body>
<?php
include('header.php');
?>
    <center><h1>CONTROLE DE UTILIZAÇÃO DE ITEMS CONTROLADOS</h1></center>



    <!-- Optional JavaScript -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>

  <ul class="list-group">
  <li class="list-group-item"><a href='inserir_movimentacao.php'>Cadastrar utilização de item</a></li>
  <li class="list-group-item"><a href='inserir_aquisicao.php'>Cadastrar movimentação de item</a>(em fase de testes do visual)</li>
  <li class="list-group-item">Relatório de utilização (não implementado no momento)</li>
</ul>

<hr>
<div class="card" style="width: 18rem;">
  <div class="card-body">
    <h5 class="card-title">Usuário</h5>
    <p class="card-text">

    <?php
      
      if (isset($_SESSION['usuario'])) {
        print($_SESSION['usuario']);
      }
      else {
        print("Não autenticado");
      }
    ?>

    </p>
    <?php
      if (isset($_SESSION['usuario'])) {
    ?>
        <a href="logout.php" class="btn btn-primary">Sair</a>
    <?php
      }
      else {
    ?>
    <a href="login.html" class="btn btn-primary">Identifique-se</a>
    <?php
      }
    ?>
  </div>
</div>
<a href="https://play.google.com/store/apps/details?id=pet.yoko.apps.cmirapp" target="_blank">
  <img src="google-playG.png">
  Versão Mobile (Android)
</a>
<?php
include('footer.php');
?>
  </body>

</html>
