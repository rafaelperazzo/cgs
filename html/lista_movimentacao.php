<?php

include('iniciar.php');

if (isset($_GET['token'])) {
    $token = $_GET['token'];
    //$username = token2Field($db,$token,"username");
    if (isset($_GET['todos'])) {
        $permissao = token2Field($db,$token,"permissao");
        if ($permissao==0) {
            $linhas = getMovimentacao($db,$token,1);
            header("Content-Type: application/json; charset=UTF-8");
            print(json_encode($linhas));    
        }
        else {
            $linhas = getMovimentacao($db,$token);
            header("Content-Type: application/json; charset=UTF-8");
            print(json_encode($linhas));
        }
    }
    else {
        $linhas = getMovimentacao($db,$token);
        header("Content-Type: application/json; charset=UTF-8");
        print(json_encode($linhas));
    }
    
}
else {
    return(json_encode (json_decode ("[]")));
}


?>