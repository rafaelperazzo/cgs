<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    <script src="tablefilter/tablefilter.js"></script>
    <link rel="stylesheet" type="text/css" href="tablefilter/style/tablefilter.css"/>
    <title>Cadastrar Item</title>

    <script> 
        $(function(){
          console.log('init');
          $("#header").load("header.html"); 
          $("#footer").load("footer.html"); 
        });
    </script> 

  </head>
  <body>
  <div id="header" style="text-align:center;"></div>
  <hr>
    <a href='https://sci02-ter-jne.ufca.edu.br/cgs/'>PÁGINA INICIAL</a>
  <hr>
  <?php
      include('iniciar.php');
      session_start();
      if (!isset($_SESSION['autenticado'])) {
        header('Location: login.html'); 
      }
      if (isset($_SESSION['permissao'])) {
        if ($_SESSION['permissao']!=0) {
          header('Location: login.html'); 
        }
      }
  ?>  
  <h1>Cadastrar Item</h1>
    
    <form action="processar_item.php" method="POST">

        <div class="form-group">
            <label for="medida">Medida</label>
            <select class="form-control" id="medida" name="medida" required>
            <?php
                $linhas = $db->select("medidas",["id","descricao"],["ORDER"=>["descricao"=>"ASC"]]);
                foreach ($linhas as $linha) {
                  $descricao = $linha['descricao'];
                  $id = $linha['id'];
                  print("<option value='$id'>$descricao</option>");
                }
              ?>
            </select>
        </div>

        <div class="form-group">
          <label for="descricao">Descrição</label>
          <input type="text" class="form-control" id="descricao" name="descricao" aria-describedby="quantidadeHelp" required>
        </div>
        
        <button type="submit" class="btn btn-primary">Cadastrar</button>

    </form>
<hr>
<h2>Items cadastrados</h2>
  <table class="table" id="tab01">
  <thead class="thead-dark">
    <tr>
      <th scope="col">ID</th>
      <th scope="col">DESCRIÇÃO</th>
      <th scope="col">MEDIDA</th>
      <th scope="col">REMOVER</th>
    </tr>
  </thead>
  <tbody>
    
    <?php
      
      $usuario = $_SESSION['usuario'];
      //["[>]medidas"=>["id"=>"id"]],
      $linhas = $db->select("items",["[>]medidas"=>["medida"=>"id"]],["items.id","items.descricao(descricao_item)","medidas.descricao(descricao_medida)"],["ORDER"=>["items.id"=>"ASC"]]);
      //print(json_encode($linhas));
      
      foreach ($linhas as $linha) {  
    ?>
    <tr>
      <?php
        $id = $linha['id'];
        $item = $linha['descricao_item'];
        $medida = $linha['descricao_medida'];
        //$data = new DateTime($linha['data']);
        //$data = $data->format('d/m/Y H:i:s');
        //$usuario = $linha['username'];
        print("<td>$id</td>");
        print("<td>$item</td>");
        print("<td>$medida</td>");
        //print("<td>$data</td>");
        //print("<td>$usuario</td>");
        $confirmacao = "Tem certeza de que deseja remover este item ?";
        print("<td><center><a href='deletar_item.php?id=$id' onclick=\"return confirm('$confirmacao')\"><img src='remove64.png'></a></center></td>");
      ?>
    </tr>
    <?php
      }
    ?>

  </tbody>
</table>
           

<script>
    var filtersConfig = {
            base_path: 'https://unpkg.com/tablefilter@latest/dist/tablefilter/',
            col_4: 'checklist',
            col_7: 'checklist',
            col_3: 'select',
            alternate_rows: true,
            col_widths: ['300px'],
            /*grid_layout: {
              width: '100%'
            },*/
            responsive: true,
            popup_filters: true,
            highlight_keywords: true,
            rows_counter: {
              text: 'Items:  '
            },
            btn_reset: {
              text: 'Limpar'
            },
            status_bar: true,
            loader: true,
            extensions: [{ name: 'sort' }]
        };
    var tf = new TableFilter('tab01', filtersConfig);
    tf.init();
    
  </script>
  <div id="footer" style="text-align:center;"></div>
  <hr>
    <a href='https://apps.yoko.pet/cgs/'>PÁGINA INICIAL</a>
  <hr>
  </body>
</html>